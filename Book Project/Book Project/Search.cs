﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace Book_Project
{
    public partial class Search : Form
    {
        Form1 form1;
        public Search(Form1 frm)
        {
            InitializeComponent();
            form1 = frm;

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SQLiteConnection sqlConnection = new SQLiteConnection();
            sqlConnection.ConnectionString = "data source = Books.db";



            string commandText = "SELECT * FROM bookList WHERE " + cmbType.Text + " LIKE'%" + txtTitle.Text + "%'";

            var datatable = new DataTable();
            SQLiteDataAdapter myDataAdapter = new SQLiteDataAdapter(commandText, sqlConnection);

            sqlConnection.Open();
            myDataAdapter.Fill(datatable);
            sqlConnection.Close();

            form1.dgvBooks.DataSource = datatable;

            
        }

        private void grpSearch_Enter(object sender, EventArgs e)
        {

        }
    }
}
