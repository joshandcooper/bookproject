﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace Book_Project
{
    public partial class Form1 : Form
    {
        //public System.Windows.Forms.DataGridView dgvBooks.Text;
        string selectedISBN = "0";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ReadData();
        }
        

        public void ReadData()
        {
            //this creates a connection to the SQL database
            SQLiteConnection sqlConnection = new SQLiteConnection();
            sqlConnection.ConnectionString = "DataSource = Books.db";

            //"data source = C:\\Users\\Cooper.M\\Desktop\\school 2019-2020\\Software\\C#\\Book Project\\Book Project\\Book Project\\bin\\Debug\\Books.db";

            //Define a SELECT Statement
            string commandText = "SELECT * FROM bookList";

            //create a Datatable to save the data in memory
            var datatable = new DataTable();

            //create a Dataadapter 
            //data adapters perform SELECT, INSERT, UPDATE and DELETE operations in the data source (database)
            //the UPDATE, INSERT and DELETE operations will be a continustion of the SELECT command
            //the data adapter uses the SELECT statment to fill a dataset and uses the other three SQL commant to transmit
            //changes back to the database

            sqlConnection.Open();
            SQLiteDataAdapter myDataAdapter = new SQLiteDataAdapter(commandText, sqlConnection);


            //fill the data from the database into the datatable
            myDataAdapter.Fill(datatable);
            sqlConnection.Close();

            //we take the data from the datatable and feed it into the datagridview
            dgvBooks.DataSource = datatable;
        }

        private void btnPos_Click(object sender, EventArgs e)
        {
            PointOfSale pointOfSale = new PointOfSale();
            pointOfSale.Show();

        }

        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Search search = new Search(this);
            search.Show();
            //opening the search function

        }

        private void newBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddBook newBook = new AddBook(this);
            newBook.Show();

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            ReadData();

        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvBooks.SelectedRows.Count > 0)
            {
                selectedISBN = dgvBooks.SelectedRows[0].Cells[0].Value.ToString();

            }
            
            if (MessageBox.Show("Are you sure?", "Confirmation", MessageBoxButtons.OKCancel) == DialogResult.OK) 
            {
                SQLiteConnection sqlConnection = new SQLiteConnection();
                sqlConnection.ConnectionString = "data source = Books.db";
                SQLiteCommand sqlCommand = new SQLiteCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = "DELETE FROM bookList WHERE ISBN=@ISBN";
                sqlCommand.Parameters.AddWithValue("@ISBN", selectedISBN);

                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                MessageBox.Show("Book has been removed", "Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information);

                ReadData();

            }
        }
    }
}
