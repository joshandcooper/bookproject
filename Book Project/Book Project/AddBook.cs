﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;


namespace Book_Project
{
    public partial class AddBook : Form
    {
        Form1 form1;
        public AddBook(Form1 frm)
        {
            InitializeComponent();
            form1 = frm;

        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            SQLiteConnection sqlConnection = new SQLiteConnection();
            sqlConnection.ConnectionString = "DataSource = Books.db";

            SQLiteCommand sqlCommand = new SQLiteCommand();

            sqlCommand.Connection = sqlConnection;
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandText = "INSERT into bookList (ISBN,BookTitle,BookAuthor,YearofPublication,Publisher,Stock,Price) Values(@ISBN,@BookTitle,@BookAuthor,@YearofPublication,@Publisher,@Stock,@Price)";
            sqlCommand.Parameters.AddWithValue("@ISBN", txtISBN.Text);
            sqlCommand.Parameters.AddWithValue("@BookTitle", txtTitle.Text);
            sqlCommand.Parameters.AddWithValue("@BookAuthor", txtAuthor.Text);
            sqlCommand.Parameters.AddWithValue("@YearofPublication", txtYear.Text);
            sqlCommand.Parameters.AddWithValue("@Publisher", txtPublisher.Text);
            sqlCommand.Parameters.AddWithValue("@Stock", txtStock.Text);
            sqlCommand.Parameters.AddWithValue("@Price", txtPrice.Text);


            sqlConnection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlConnection.Close();
            MessageBox.Show("Book Added", "New Book", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();


        }
    }
}
