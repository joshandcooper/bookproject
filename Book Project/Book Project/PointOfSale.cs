﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;

namespace Book_Project
{
    public partial class PointOfSale : Form
    {
        string selectedBook = "0";
        double price = 0.0;
        double changeRequired = 0.0;
        double amountTendered = 0.0;
        string textpath = @"text.txt";


            //"Users\\coope\\Documents\\bookproject\\Book Project\\Book Project\\bin\\Debug\\text.txt";
            
        public PointOfSale()
        {
            InitializeComponent();
            ReadData();
            

        }

        private void RecordNewSale()
        {
            StreamWriter sw = new StreamWriter(textpath, true);
            sw.WriteLine("\n" + txtPos.Text + "\t" + txtPrice.Text + "\t" + txtTendered.Text + "\t" + txtChange.Text);
            MessageBox.Show("Receipt Recorded", "Transation Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
            sw.Close();
        }

        public void ReadData()
        {
            //this creates a connection to the SQL database
            SQLiteConnection sqlConnection = new SQLiteConnection();
            sqlConnection.ConnectionString = "DataSource = Books.db";
                       
            string commandText = "SELECT * FROM bookList";
                        
            var datatable = new DataTable();

           
            sqlConnection.Open();
            SQLiteDataAdapter myDataAdapter = new SQLiteDataAdapter(commandText, sqlConnection);
                                   
            myDataAdapter.Fill(datatable);
            sqlConnection.Close();

            dgvPos.DataSource = datatable;

                        
        }

        private void PointOfSale_Load(object sender, EventArgs e)
        {
            //this creates a connection to the SQL database
            SQLiteConnection sqlConnection = new SQLiteConnection();
            sqlConnection.ConnectionString = "DataSource = Books.db";

            //"data source = C:\\Users\\Cooper.M\\Desktop\\school 2019-2020\\Software\\C#\\Book Project\\Book Project\\Book Project\\bin\\Debug\\Books.db";

            //Define a SELECT Statement
            string commandText = "SELECT * FROM bookList";

            //create a Datatable to save the data in memory
            var datatable = new DataTable();

            
            sqlConnection.Open();
            SQLiteDataAdapter myDataAdapter = new SQLiteDataAdapter(commandText, sqlConnection);


            //fill the data from the database into the datatable
            myDataAdapter.Fill(datatable);
            sqlConnection.Close();

            //we take the data from the datatable and feed it into the datagridview
            dgvPos.DataSource = datatable;
        }

        private void btnBuy_Click(object sender, EventArgs e)
        {
            RecordNewSale();
                        
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            SQLiteConnection sqlConnection = new SQLiteConnection();
            sqlConnection.ConnectionString = "data source = Books.db";



            string commandText = "SELECT * FROM booklist WHERE " + cmbSearchType.Text + " LIKE'%" + txtPos.Text + "%'";

            var datatable = new DataTable();
            SQLiteDataAdapter myDataAdapter = new SQLiteDataAdapter(commandText, sqlConnection);

            sqlConnection.Open();
            myDataAdapter.Fill(datatable);
            sqlConnection.Close();

            dgvPos.DataSource = datatable;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ReadData();

        }

        private void cmbSearchType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dgvPos_SelectionChanged(object sender, EventArgs e)
        {
            SQLiteConnection sqliteConnection = new SQLiteConnection();
            sqliteConnection.ConnectionString = "data source = Books.db";

            if (dgvPos.SelectedRows.Count > 0)
            {
                selectedBook = dgvPos.SelectedRows[0].Cells[0].Value.ToString();
                string commandText = "SELECT * FROM booklist WHERE ISBN=" + selectedBook;
                var datatable = new DataTable();
                SQLiteDataAdapter myDataAdapter = new SQLiteDataAdapter(commandText, sqliteConnection);
                sqliteConnection.Open();
                myDataAdapter.Fill(datatable);

                txtPrice.Text = datatable.Rows[0]["Price"].ToString();
                txtPos.Text = datatable.Rows[0]["BookTitle"].ToString();

                sqliteConnection.Close();

            }
        }

        private void txtTendered_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            //this part makes it that the $ within the price is ignored as it wouldn't work otherwise
            string substring = txtPrice.Text;
            string[] words = substring.Split('$');
            string newWord = "";
            foreach (string word in words)
            {
                newWord += word;

            }
            txtPrice.Text = newWord;


            price = Convert.ToDouble(txtPrice.Text);
            amountTendered = Convert.ToDouble(txtTendered.Text);
            changeRequired = amountTendered - price;
            txtChange.Text = "$" + Convert.ToString(changeRequired);
        }

        private void instructionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Use the Drop-down menu box to pick a search type, then use the box below to search, press 'select'. Then enter ammount tendered, Press 'go' and then press 'Buy'", "Help", MessageBoxButtons.OK, MessageBoxIcon.Information);
            
        }
    }
}
